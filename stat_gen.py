import json, sys
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier
from collections import defaultdict
import numpy as np
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
from datetime import datetime

class bitly:
	def __init__(self):
		self.data = np.asarray([])
		self.labels = []
		self.countries = defaultdict(float)
		self.length = 88.8939615831
		self.timezones = defaultdict(float)
		self.OS = defaultdict(float)
		self.OSlist = ["Android", "Windows", "iPhone", "iPad", "Ubuntu"]
		self.st = datetime.now()
	
	def get_stats(self):
		data = open('decodesaa','r')
		labels = open('labels.dat','w')
		countries = open('countries.dat','w')
		timezones = open('timezones.dat','w')
		OS = open('OS.dat','w')
		for line in data:
			found = False
			j = json.loads(line)
			if "c" in j:
				self.countries[j["c"]] += 1
			else:
				self.countries["unknown"] += 1
			self.length += len(j["u"])
			if "tz" in j:
				self.timezones[j["tz"]] += 1
			else:
				self.timezones["unknown"] += 1
			self.labels.append(j["nk"])
			for word in self.OSlist:
				if (j["a"].find(word) > 0): 
					self.OS[word] += 1
					found = True
			if not found:
				self.OS["unknown"] += 1
		print("Average length: " + str(self.length/3137627))
		for i in range(0,len(self.labels)):
			labels.write(str(self.labels[i])+"\n")
		for k in self.countries.keys():
			countries.write(k+" "+str(self.countries[k]/3137627)+"\n")
		for k in self.OS.keys():
			OS.write(k+" "+str(self.OS[k]/3137627)+"\n")
		for k in self.timezones.keys():
			timezones.write(k+" "+str(self.timezones[k]/3137627)+"\n")

	def readToDict(self,fp, dest):
		for line in fp:
			tmp = line.strip().split()
			dest[tmp[0]] = float(tmp[1])
	
	def build(self):
		data = open('decodesaa','r')
		labels = open('labels.dat','r')
		countries = open('countries.dat','r')
		self.readToDict(countries,self.countries)
		timezones = open('timezones.dat','r')
		self.readToDict(countries,self.countries)
		OS = open('OS.dat','r')
		self.readToDict(OS,self.OS)
		# X = open('data.save','w')
		for line in data:
			j = json.loads(line)
			l = len(j["u"])/self.length
			a = None
			for word in self.OSlist:
				if (j["a"].find(word) > 0): 
					a = self.OS[word]
				else:
					a = self.OS["unknown"]
			if "c" in j:
				c = self.countries[j["c"]]
			if "tz" in j:
				tz = self.timezones[j["tz"]]
			np.x = np.asarray([l,a,c,tz])
			if self.data.size == 0:
				self.data = np.x
			else:
				self.data = np.row_stack((self.data,np.x))
		np.save("data.save",self.data)

	def num(self):
		
		count = 0
		data = open('decodesaa','r')
		for line in data:
			count += 1
		return count

	def load(self):
		l = open('labels.dat','r')
		for line in l:
			self.labels.append(int(line.strip()))
		self.labels = np.asarray(self.labels)
		self.data = np.load('data.save.npy')

	def train(self):
		clf = RandomForestClassifier(n_estimators = 100)
		x_train, x_test, y_train, y_test = train_test_split(self.data, self.labels, test_size=0.33, random_state=42)
		clf.fit(x_train, y_train)
		print clf.score(x_test, y_test)

	def time(self):
		print datetime.now() - self.st

b = bitly()
b.load()
b.train()
b.time()


